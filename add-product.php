<!DOCTYPE html> 
<html>
<head>
<link type="text/css" rel="stylesheet" href="add-product.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.all.min.js"></script>
<script src="/js/swalert.js"></script>
<script src="/js/sweetalert.min.js"></script>

<meta charset="utf-8">
<title>ADD PRODUCT</title>
<meta name="viewport" content="width=device-width">
</head>

<body>
<header>
<p>Product Add</p>
<button onclick="window.location.href = 'index.php';">Cancel</button>
<hr>
</header>

<main>
<?php
// Add the files are needed to connect to the database and files with objects
include_once 'database.php';
include_once 'product.php';
include_once 'category.php';
// Get connect with database
$database = new Database();
$db = $database->getConnection();
// Create object of Category class
$category = new Category($db);
?>

<?php
// If the form was sent
if ($_POST) {
    // Get category
    $categ = $_POST['category_id'];
    // Create the product of specific clas depending on the category
    $product = ($categ==1) ? new dvd($db) : (($categ==2) ? new book($db) : new furniture($db));
    // Set values for properties of product
    $product->sku = $_POST['sku'];
    $product->name = $_POST['name'];
    $product->price = $_POST['price'];
    $product->category_id = $_POST['category_id'];
    $product->size = $_POST['size'];
    $product->weight = $_POST['weight'];
    $product->height = $_POST['height'];
    $product->width = $_POST['width'];
    $product->length = $_POST['length'];
    
    // Create product
    if ($product->create())
    {?>
        <script>
        Swal.fire({icon: "success", title: "The product was successfully created", showConfirmButton: false, timer: 3000 });
        setTimeout(function(){ window.location.href = 'index.php'; }, 3 * 1000);
        </script>
        <?php
    }
    else {?>
        <script>
        Swal.fire({ icon: "error", title: "This product cannot be created!", showConfirmButton: false, timer: 3000 })
        </script>
        <?php
    }
}
?>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" id="product_form" name="product_form" pattern="([0-9])"
title="This is an error message">         
    <div class="main">
        <p><label>SKU</label><input id="sku" type="text" name="sku"></p>
        <p><label>Name</label><input id="name" type="text" name="name"></p>
        <p><label>Price ($)</label> <input id="price" type="text" name="price" pattern="[0-9]*[.,]?[0-9]+" oninvalid="InvalidMsg(this);"  oninput="InvalidMsg(this);"></p>
        
        <p><label>Type Switcher</label>
        <?php
        $stmt = $category->read();?>
        <!--Place the category in the drop-down list-->
        <select id='productType' name='category_id' onchange='showBlocks(this.value)'>";<?php
        echo "<option>Choose category...</option>";
  
        while ($row_category = $stmt->fetch(PDO::FETCH_ASSOC)) {
            extract($row_category);
            echo "<option value='{$id}' name='{$name}'>{$name}</option>";  
        }
        echo "</select>";
        ?></p>
        
        <div id="DVD" style="display:none;">Please, provide size in MB
            <p>Size (MB)<input id="size" type="text" name="size" pattern="[0-9]*[.,]?[0-9]+" oninvalid="InvalidMsg(this);"></p>
        </div>
        
        <div id="Furniture" style="display:none;">Please, provide dimensions in HxWxL format
            <p>Height (CM)<input id="height" type="text" name="height" pattern="[0-9]*[.,]?[0-9]+" oninvalid="InvalidMsg(this);"></p>
            <p>Width (CM)<input id="width" type="text" name="width" pattern="[0-9]*[.,]?[0-9]+" oninvalid="InvalidMsg(this);"></p>
            <p>Length (CM)<input id="length" type="text" name="length" pattern="[0-9]*[.,]?[0-9]+" oninvalid="InvalidMsg(this);"></p>
        </div>
        
        <div id="Book" style="display:none;">Please, provide weight in KG
            <p>Weight (KG)<input id="weight" type="text" name="weight" pattern="[0-9]*[.,]?[0-9]+" oninvalid="InvalidMsg(this);"></p>
        </div>
        <button class="show-alert" type="submit" onclick="return validate_form();">Save</button>
    </div>
</form>

</main>

<footer>
<hr>
<p>Scandiweb Test assignment</p>
</footer>

<script>
// Show blocks depending on the category
function showBlocks(val){
    var categs = ['DVD', 'Book', 'Furniture'];  
    for(let i = 0; i < categs.length; i++)
        document.getElementById(categs[i]).style.display = (i == val-1 ? 'block' : 'none');  
};
</script>

<script>
// Check form on empty fields
   function validate_form() {              
    var frm = document.product_form;
    var isFullFields = (document.product_form.productType.selectedIndex > 0);
    for(i = 0; i < 3; i++){
            isFullFields &= (document.getElementsByTagName('input')[i].value != "");
    }
    isFullFields &= (checkFields(getSelectedFields(getSelectedType('productType'))));
    if(!isFullFields)   modalWin("error", "Please, submit required data", false, 3000);
    return (isFullFields) ? true : false;
   }
// Check form on incorrect values
function InvalidMsg(textbox) {  
    (textbox.validity.patternMismatch) ? textbox.setCustomValidity('Please, provide the data of indicated type') :
     textbox.setCustomValidity('');
    return true;
}
// Get category by id
function getSelectedType(id){
    var a = document.getElementById(id);
    for (i=1; i<a.options.length; i++) 
        if (a.options[i].selected)
            return (a.options[i].innerHTML);
    return "";
}
// Get fields of concrete category
function getSelectedFields(type){
    var tag = document.getElementById(type);
    var elems = (type!="") ? tag.getElementsByTagName('input') : "";
    var fields = [];
    for(j=0; j<elems.length; j++) fields.push(elems[j].id);
    return fields;
}
// Check fields of concrete category
function checkFields(fields){
    var isEmptyFields = false;
    if(fields.length==0) isEmptyFields = true;
    for(i = 0; i < fields.length; i++)
        isEmptyFields |= (document.getElementById(fields[i]).value == "");
    if (isEmptyFields)
        modalWin("error", "Please, submit required data", false, 3000);
    return !isEmptyFields;
}
// Show modal window
function modalWin(){
    Swal.fire({
        icon: arguments[0],
        title: arguments[1],
        showConfirmButton: arguments[2],
        timer: arguments[3]
    })
}
</script>
</body>
</html>