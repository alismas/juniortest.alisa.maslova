<?php
// Abstract class for all types of prosucts
abstract class Product {
    public $conn;
    public $table_name = "products";
    public $query_add;   
    public $id;
    public $sku;
    public $name;
    public $price;
    public $category_id;
    public $stmt;

    abstract public function create(); 

    public function __construct($db) {
        $this->conn = $db;
        // Request an entry insertion to the database (only general fields, the others will be added for each class)
        $this->query_add = "INSERT INTO " . $this->table_name . " SET sku=:sku, name=:name, price=:price,  category_id=:category_id";
    }
    
    // Get all products from database   
    public function readAll() { 
        $query = "SELECT sku, name, price, category_id, size, height, width, length, weight FROM " . $this->table_name . " ORDER BY id DESC ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }
    // Read values which were input to form (general for all types of products)
    function readHtml(...$params){
        $this->sku=htmlspecialchars(strip_tags($this->sku));
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->price=htmlspecialchars(strip_tags($this->price));
        $this->category_id=htmlspecialchars(strip_tags($this->category_id));
        
        foreach($params as $param)
            $this->{$param}=htmlspecialchars(strip_tags($this->{$param}));
    }
    // Binding the parameters of the request to variable (general for all types of products)
    function bindParams(...$params){
        $this->stmt->bindParam(":sku", $this->sku);
        $this->stmt->bindParam(":name", $this->name);
        $this->stmt->bindParam(":price", $this->price);
        $this->stmt->bindParam(":category_id", $this->category_id);
        
        foreach($params as $param)
            $this->stmt->bindParam(":$param", $this->{$param});    
    }
}
// Class DVD product
class dvd extends Product 
{
    public $size;
    public function create(){
        $query = $this->query_add . ", size=:size";
        $this->stmt = $this->conn->prepare($query);

        $this->readHtml('size');
        $this->bindParams('size');
        return $res = ($this->stmt->execute()) ? true : false;
    }
}
// Class Book product
class book extends Product 
{
    public $weight;
    public function create(){
        $query = $this->query_add . ", weight=:weight";    
        $this->stmt = $this->conn->prepare($query);

        $this->readHtml('weight');
        $this->bindParams('weight');
        return $res = ($this->stmt->execute()) ? true : false;
    }
}
// Class Furniture product
class furniture extends Product 
{
    public $height;
    public $width;
    public $length;
    public function create(){
        $query = $this->query_add . ", height=:height, width=:width, length=:length"; 
        $this->stmt = $this->conn->prepare($query);

        $this->readHtml('height', 'width', 'length');
        $this->bindParams('height', 'width', 'length');
        return $res = ($this->stmt->execute()) ? true : false;
    }
}
// Class used for the output of products of all types
class products extends Product {
    public function create(){}
}