<!DOCTYPE html> 
<html>
<head>
<link type="text/css" rel="stylesheet" href="add-product.css"/>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.all.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/js/swalert.js"></script>
<script src="/js/sweetalert.min.js"></script>

<meta charset="utf-8">
<title>PRODUCT LIST</title>
<meta name="viewport" content="width=device-width">
</head>

<body>
<?php
// Add the files are needed to connect to the database and files with objects
include_once 'database.php';
include_once 'product.php';
include_once 'category.php';
// Get connect with database
$database = new Database();
$db = $database->getConnection();
// Create object of products и Category classes
$category = new Category($db);
$products = new products($db);
// Read products from database
$stmt = $products->readAll();
?>
    
<header>
<p>Product List</p>
<div><button onclick="window.location.href = 'add-product.php';">ADD</button></div>
<hr>
</header>

<main>
<?php
// Output products
echo '<form action="'.$_SERVER['PHP_SELF'].'" method="POST">';?>
    <div id="all"><?php
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            extract($row);?>
             <div id="block">
                <?php 
                echo '<input type="checkbox" class="delete-checkbox" name="delete-checkbox[]" 
                value="'.$row['sku'].'" method="POST">';
                echo "<p>{$sku}</p>";
                echo "<p>{$name}</p>";
                echo "<p>{$price}" . " $" . "</p>";
                $prod_type = ["Size: {$size} MB", 
                            "Weight: {$weight}  KG", 
                            "Dimension: {$height}х{$width}х{$length}" ];
                echo "<p>" . $prod_type[$category_id-1] . "</p>";
                ?>
            </div>
<?php   } ?> 
        <button type="submit" id="delete-product-btn" name="submitForm">MASS DELETE</button>
   </div>
</form>
<?php
// If products for delete were choose - delete all, which were choose
if ( isset ( $_POST['delete-checkbox'] ) )  
{
    $ids = implode("','",$_POST['delete-checkbox']);
    $query = "DELETE FROM `products` WHERE `sku` IN ('".$ids."')";
    $db->query($query);
    exit("<meta http-equiv='refresh' content='0; url= /index.php'>");
} ?>
</main>

<footer>
<hr>
<p>Scandiweb Test assignment</p>
</footer>

</body>
</html>