<?php
class Category {
    // Connect to database and name of table
    private $conn;
    private $table_name = "categories";
    // Properties of object
    public $id;
    public $name;

    public function __construct($db) {
        $this->conn = $db;
    }
    // This method is used in the drop-down list
    function read() {
        $query = "SELECT id, name FROM " . $this->table_name . " ORDER BY id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }
}
?>